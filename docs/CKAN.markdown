# CKAN = Comprehensive Knowledge Archive Network
Développée par l’Open Knowledge Foundation (OKFN). Organisation, basée à Londres, chapeaute au niveau mondial les mouvements pour l’ouverture des données.
![](./files/OpenKnowledge.png)
						
Projet CKAN démarré en 2007 (à titre de comparaison OpenDataSoft fondée en 2011)

#### Références
<https://okfn.org/>


